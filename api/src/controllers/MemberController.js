import { addMember, editMember, listMembers, getMember, getMemberByProjectId, deleteMember, getMembersAndProjects } from '../models/MemberModel';

export function add(req, res) {
    addMember(req.body.name, req.body.telephone, req.body.term, req.params.project_id, req.body.level_id).then((response) => {
        res.status(200).send({ message: 'Membro adicionado com sucesso!'});
    }).catch((error) => {
        res.status(500).send({ message: 'Erro ao adicionar membro!', error});
    });
}

export function edit(req, res) {
    editMember(req.params.id, req.body.name, req.body.telephone, req.body.term).then((response) => {
        res.status(200).send({ message: 'Membro editado com sucesso!'});
    }).catch((error) => {
        res.status(500).send({ message: 'Erro ao editar membro', error});
    });
}

export function list(req, res) {
    listMembers().then((response) => {
        res.status(200).send(response);
    }).catch((error) => {
        res.status(500).send({ message: 'Erro ao listar membros! ', error});
    })
}

export function search(req, res) {
    getMember(req.params.id).then((response) => {
        res.status(200).send(response);
    }).catch((error) => {
        res.status(500).send({ message: 'Erro ao encontrar membro!', error});
    })
}

export function del(req, res) {
    deleteMember(req.params.id).then((response) => {
        res.status(200).send({ message: 'Membro excluido com sucesso!'});
    }).catch((error) => {
        res.status(500).send({ message: 'Erro ao excluir membro!', error})
    })
}

export function getMemberByProject(req, res) {
    getMemberByProjectId(req.params.id).then((response) => {
        res.status(200).send(response);
    }).catch((error) => {
        res.status(500).send({ message: 'Erro ao encontrar membros!', error});
    });
}

export function getmembersprojects(req, res) {
    getMembersAndProjects().then((response) => {
        res.status(200).send(response);
    }).catch((error) => {
        res.status(500).send({ message: 'Erro ao buscar membros e projetos ', error });
    });
}