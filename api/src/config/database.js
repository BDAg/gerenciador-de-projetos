import MySQL from 'mysql';

const connection = MySQL.createPool({
    host: 'localhost',
    user: 'root',
    password: 'chicao',
    database: 'gerenciador',
    connectionLimit: 10,
});

export default connection;
