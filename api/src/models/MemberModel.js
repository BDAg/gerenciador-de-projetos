import db from '../config/database';


export function addMember (name, telephone, term, project_id, level_id) {
    const query = `INSERT INTO member (name, telephone, term, project_id, level_id) VALUES ('${name}', '${telephone}', '${term}', ${project_id}, ${level_id})`;

    return new Promise((resolve, reject) => {
        db.query(query, (err, result) => {
            if(err){ 
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
}

export function editMember(id, name, telephone, term) {
    const query = `UPDATE member SET name='${name}', telephone='${telephone}', term='${term}' WHERE id = ${id}`;

    return new Promise((resolve, reject) => {
        db.query(query, (err, result) => {
            if(err){
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
}

export function listMembers() {
    const query = `SELECT * FROM member`;

    return new Promise((resolve, reject) => {
        db.query(query, (err, result) => {
            if(err){
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
}

export function getMember(id) {
    const query = `SELECT * FROM member WHERE id =${id}`;

    return new Promise((resolve, reject) => {
        db.query(query, (err, result) => {
            if(err) 
                reject(err);
            else 
                resolve(result);
            
        });
    });
}

export function deleteMember(id) {
    const query = `DELETE FROM member WHERE id =${id}`;

    return new Promise((resolve, reject) => {
        db.query(query, (err, result) => {
            if(err)
                reject(err);
            else    
                resolve(result);
        });
    });
}

export function getMemberByProjectId(project_id){
    const query = `SELECT * FROM member WHERE project_id =${project_id}`;

    return new Promise((resolve, reject) => {
        db.query(query, (err, result) => {
            if(err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
}

export function getMembersAndProjects() {
    const query = `SELECT member.id, project.name AS projeto, member.name FROM member JOIN project ON project.id = member.project_id`;

    return new Promise ((resolve, reject) => {
        db.query(query, (err, result) => {
            if(err)
                reject(err)
            else
                console.log(result);
                resolve(result);
        });
    });
}