import { Router } from 'express';
import { add, edit, list, search, del, getMemberByProject, getmembersprojects } from '../controllers/MemberController';
import Access from '../middleware/VerifyToken';

const Route = Router ();

Route.post('/:project_id', Access(3), add);
Route.put('/:id', Access(3), edit);
Route.get('/', Access(3), list);
Route.get('/:id', Access(3), search);
Route.delete('/:id', Access(3), del);
Route.get('/:id/pj', Access(3), getMemberByProject);
Route.get('/list/projects', Access(3), getmembersprojects);

export default Route;

