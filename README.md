# Gerenciador de projetos


![logo](https://trello-attachments.s3.amazonaws.com/5c75cbd14774ab6d8ef9a6c8/5cd5f4b2e6809f67963c1be0/0b26a68a9de5e1f9d077f0ab9935b22b/Untitled-1.png)


## Um sensacional projeto que sera implementado para facilitar o gerenciamento de projetos dentro da faculdade

# ↓ Identificador de execuções do projeto ↓

### Documentação

- [x] Definições do Projeto

- [x] Definições da Solução

### Desenvolvimento

- [x] Sprint 1 (API)
- [x] Sprint 2 (Back-end)
 
- [x] Sprint 3 (Front-end)
- [x] Sprint 4 (Integração do Sistema)

- [x] Finalização (Encerramento do Projeto)



### Tecnologias utilizadas




* *JavaScript*

* *NodeJS*

* *MySQL*

* *Visual Studio Code*

* *Linux/Windows*

* *HTML5 & CSS3* 

* *ReactJS*
 


