import React, { Component } from 'react';
import Modal from 'react-modal';
import './Modal.css';

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};

Modal.setAppElement("#root");

class AppModal extends Component {
    constructor() {
        super();

        this.state = {
            modalIsOpen: false
    };

        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    render() {
        return (
            <div>
            <button className="modal-button" onClick={this.openModal}>{this.props.titlebutton}</button>

            <Modal
                isOpen={this.state.modalIsOpen}
                onAfterOpen={this.afterOpenModal}
                onAfterClose={this.props.action}
                onRequestClose={this.closeModal}
                style={customStyles}
                contentLabel="Example Modal"
                >

                <h2 ref={subtitle => this.subtitle = subtitle}>{this.props.subtitle}</h2>

                {this.props.children}
                <button className="form-cancel-button"onClick={this.closeModal}>Fechar</button>
            </Modal>
            </div>
        );
    }
}

export default AppModal;