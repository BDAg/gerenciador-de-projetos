import React, {Component} from 'react';
import './Input.css';

export class Input extends Component {
    
    
    render(){
        return(
                <input
                     type={this.props.type}
                     placeholder={this.props.placeholder} 
                     name={this.props.name}
                     id={this.props.id}
                     style={{width: `${this.props.width}`, height:`${this.props.height}`}}
                     pattern={this.props.pattern}
                     required={this.props.required}
                />        
        )
    }
}