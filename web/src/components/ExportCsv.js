import React, { Component } from 'react';
import Axios from 'axios';
import { JsonToExcel } from 'react-json-excel';

class ExportCsv extends Component {

    constructor(props) {
        super(props);
        this.state = {
            projects: [],

            filename: 'Projetos/Membros',
            fields: { 
              'id': 'Index',
              'name': 'Nome do Projeto',
              'description': 'Descricao do Projeto',
              'members' : 'Membros do Projeto'
            },
            style: {
              padding: "5px",
              margin: "5px"
            },
            data: [],
        }
    }

    getProjects() {
        Axios({
            method: 'GET', 
            url: 'http://142.4.197.81:8080/v1/projects/', 
            headers: { 'x-access-token' : localStorage.getItem('Token') }
        }).then((response) => {
            this.setState({
                data: response.data,
            })
        }).catch((error) => {
            console.log(error);
        })
    }

    getMembers() {
        Axios({
            method: 'GET',
            url: 'http://142.4.197.81:8080/v1/members/9/pj',
            headers: {'x-access-token' : localStorage.getItem('Token') }
        }).then((response) => {
            console.log(response.data)
            this.setState({
                members: response.data,
            })
        }).catch((error) => {
            console.log(error);
        })
    }

    componentDidMount() {
        this.getProjects();
        this.getMembers();     
    }
 
    render() { 
        if (this.state.data.length>0) {
            return(
                <div> 
                    <JsonToExcel
                        data={this.state.data}
                        className={this.state.className}
                        filename={this.state.filename}
                        fields={this.state.fields}
                        style={this.state.style}
                        separator=';'                       
                        />
                </div>
            ); 
        } else {
            return (
                <div className="container">
                    <div className="loading"/>
                </div>
            );
        }
    }
}

export default ExportCsv;