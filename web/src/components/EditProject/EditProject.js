import React, { Component } from 'react';
import Axios from 'axios';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './EditProject.css';

toast.configure()

class EditProjectPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: '',
            name: '',
            description: '',
            git: '',
            project: this.props.project
            
        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
        console.log(this.state.git);
    }
    
    getProject() {
        Axios({
            method: 'GET',
            url: `http://142.4.197.81:8080/v1/projects/${this.state.project}`,
            headers: { 'x-access-token': localStorage.getItem('Token') },
        }).then((response) => {
            this.setState({
                id: response.data[0].id,
                name: response.data[0].name,
                description: response.data[0].description,
                git: response.data[0].link
            });
            document.getElementById('desc').value = response.data[0].description;
            console.log(this.state.project);
        }).catch((error) => {
            console.log(error);
        });
    }

    updateProject() {
        // console.log(this.props);
        // this.props.history.push('/lista-projetos');
        Axios({
            method: 'PUT',
            url: `http://142.4.197.81:8080/v1/projects/${this.state.project}`,
            headers: { 'x-access-token': localStorage.getItem('Token') },
            data: this.state,
        }).then((response) => {
            toast.success(response.data.message, { position: toast.POSITION.BOTTOM_CENTER });
            this.props.history.push('/lista-projetos');
        }).catch((error) => {
            console.log(error);
            toast.error(error.response.data.message, { position: toast.POSITION.BOTTOM_CENTER });
        });
    }

    componentDidMount() {
        this.getProject();
    }
    redirectPage() {
        this.props.history.push('/lista-projetos');
    }
    render() {
        return(
            <div className="container">
                <div className="form-contact">              
                    <div>
                        <label className="id-project">Projeto: {this.state.id}</label>
                    </div><br/>
                    <div>
                        <h4>Nome</h4>
                        <input className="form-contact-input" name="name" defaultValue={this.state.name} onChange={this.handleChange}/>
                    </div>
                    <div>
                        <h4>Link do Git</h4>
                        <input className="form-contact-input" name="git" defaultValue={this.state.git} onChange={this.handleChange}/>
                    </div>
                    <div>
                        <h4>Descrição</h4>
                        <textarea id="desc" className="form-contact-textarea" name="description" defaultValue={this.state.description} onChange={this.handleChange}/>
                    </div>
                    <div>
                        <button className="form-contact-button" onClick={ this.updateProject.bind(this) }>Enviar</button>
                        <button className="form-cancel-button" onClick={ this.redirectPage.bind(this) }>Voltar</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default EditProjectPage;