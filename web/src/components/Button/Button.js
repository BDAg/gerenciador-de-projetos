import React, { Component } from 'react';
import './Button.css';

export class CreateButton extends Component{
    render(){
        return(

            <a  
                onClick={this.props.onClick}
                className="btn btn-medium btn-black btn-radius" >{this.props.text}
            </a>
        )
    }
}
export class ConfirmButton extends Component{
    render(){
        return(

            <a  
                onClick={this.props.onClick}
                className="btn btn-medium btn-green btn-radius" >{this.props.text}
            </a>
        )
    }
}
export class CancelButton extends Component{
    render(){
        return(
            <a  
                onClick={this.props.onClick}
                className="btn btn-medium btn-red btn-radius" >{this.props.text}
            </a>     
        )
    }
}
export class EditButton extends Component{
    render(){
        return(
            <a  
                onClick={this.props.onClick}
                className="btn btn-medium btn-blue btn-radius" >{this.props.text}
            </a>     
        )
    }
}