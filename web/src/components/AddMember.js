import React, { Component } from 'react';
import Axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure()

class AddMember extends Component {
    constructor(props) {
        super(props);

        this.state ={ 
            name: '',
            term: '',
            telephone: '',
            level_id: 4,
        };
        
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
        console.log(this.state);
    }

    limparCampos() {
        document.getElementById('name').value = '';
        document.getElementById('telephone').value = '';
        document.getElementById('termo').value = 0;
        this.setState({
            name: '',
            telephone: '',
            term: ''
        })
    }

    addMember() {
        if(this.state.name.length > 0 && this.state.telephone.length > 0 && this.state.term !== '') {
            Axios({
                method: 'POST',
                url: `http://142.4.197.81:8080/v1/members/${this.props.project}`,
                headers: { 'x-access-token': localStorage.getItem('Token') },
                data: this.state
            }).then((response) => {
                toast.success(response.data.message, { position: toast.POSITION.BOTTOM_CENTER });
                this.limparCampos();
            }).catch((error) => {
                console.log(error);
                toast.error(error.response.data.message);
            });
        } else {
            toast.error('Preencha todos os campos', { position: toast.POSITION.BOTTOM_CENTER });
        }
    } 

    render() {
        return(
            <div>
                <h4>Membro</h4>
                <input className="form-contact-input" id="name" size="50" name="name" placeholder="Nome" onChange = {this.handleChange}/>
                <div className="form-contact-box">
                    <input className="form-contact-input-telephone" size="50" id="telephone" name="telephone" placeholder="Telefone" onChange = {this.handleChange}/>                 
                        <select id="termo"name="term" onChange = {this.handleChange}>
                            <option value="0"selected>Termo</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                        </select>
                </div><br/>
                <button className="form-contact-button" onClick={() => this.addMember()}>Enviar</button>
            </div>
        );
    }
    
}

export default AddMember;