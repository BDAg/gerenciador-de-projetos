import React, { Component } from 'react';
import './Input.css';

export default class EmailInput extends Component{
    render(){
        return(
            <input className="InputEmail" type="text" name="email" />  
        )
    }
}

export class PasswordInput extends Component{
    render(){
        return(
            <input className="InputPassword" type="password" name="password"/>  
        )
    }
}
