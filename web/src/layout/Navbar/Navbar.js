import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './Navbar.css'

export default class Navbar extends Component {
    render() {
        return (
            <div id="navbar">
                <nav>
                    <ul>
                        <li><a href="/cadastro-projeto">Novo projeto</a></li>
                        <li><a href="/lista-projetos">Lista de projetos</a></li>
                        <li><a href="/csv">Exportar para CSV</a></li>
                    </ul>
                </nav>
            </div>
        )
    }
}

