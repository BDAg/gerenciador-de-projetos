import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Navigation extends Component {

    render() {
        return(
            <nav id="menu">
                <div className="container"> 
                    <div><NavLink to="/cadastro-projeto">Cadastrar Tema</NavLink></div>
                    <div><NavLink to="/lista-projetos">Lista de Projetos</NavLink></div>
                </div>
            </nav>
        );
    }
}

export default Navigation;