import React, { Component } from 'react';
import './Layout.css';
import Topbar from './Header/Topbar/Topbar';
import Navbar from './Navbar/Navbar';
import Footer from './Footer/Footer';

export default class Layout extends Component {
    render(){
        return(
            <div className="Layout" style={{backgroundColor: "#F9F9F2"}} > 
                <header>
                    <Topbar/>
                    <Navbar />
                </header>

                <div style={{width: "100%",height: "80%", marginTop:"5.7%" }}>
                    {this.props.children}
                </div>
                
                <div style={{width: "100%",height: "10%", marginTop: "11%", backgroundColor:"#F9F9F2"}}>
                    <Footer />
                </div>

            </div>
        );
    }
}
