import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import ExportCsvPage from './components/ExportCsv';
import CadastroProjetoPage from './pages/Cadastro/CadastroProjeto';
import ListProjectsPage from './pages/Listar/ProjectList';
import LoginPage from './pages/LoginPage/LoginPage';
import Layout from './layout/Layout';
import ProjectEditPage from './pages/ProjectEdit';

const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
	<Route {...rest} render={props => (<Layout><Component {...props} /></Layout>)} />
)

class App extends Component {
    render() {
        return (
            //aqui estamos criando as rotas pros components
            <Switch>
                <AppRoute exact path="/csv" layout={Layout}component = {ExportCsvPage} />
                <AppRoute exact path="/cadastro-projeto" layout={Layout} component={CadastroProjetoPage}/>
		        <AppRoute exact path="/lista-projetos" layout={Layout} component = {ListProjectsPage}/>
                <AppRoute exact path="/editproject/:project_id" layout={Layout} component = {ProjectEditPage}/> 
                <Route exact path="/" component={LoginPage}/> 
            </Switch>
        );
    }
}

export default App;
//testando commit dnv