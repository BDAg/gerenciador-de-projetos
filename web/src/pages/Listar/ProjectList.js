import React, { Component } from 'react';
import Axios from 'axios';
import Modal from '../../components/Modal/Modal';
import AddMember from '../../components/AddMember';
import { NavLink } from 'react-router-dom';
import './ProjectList.css';

class ListProjectsPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
        }
    }

    getProjects() {
        Axios({
            method: 'GET', 
            url: 'http://142.4.197.81:8080/v1/projects/members', 
            headers: { 'x-access-token' : localStorage.getItem('Token') }
        }).then((response) => {
            this.setState({
                data: response.data,
            })
        }).catch((error) => {
            console.log(error);
        })
    }

    componentDidMount() {
        // this.getProjects();  
        setTimeout(()=> {
            this.getProjects();
        }, 500)  
    }
    
    deleteMember(project_id) {
        if (window.confirm('Deseja excluir esse membro?')) {
            Axios({ 
                method: 'DELETE',
                url: `http://142.4.197.81:8080/v1/members/${project_id}`,
                headers: { 'x-access-token': localStorage.getItem('Token') }
            }).then((response) => {
                this.getProjects();
            }).catch((error) => {
                console.log(error);
            })
        }
    }

    deleteProject(project_id) {
        if (window.confirm('Deseja excluir esse projeto?')) {
            Axios({
                method: 'DELETE',
                url: `http://142.4.197.81:8080/v1/projects/${project_id}`,
                headers: { 'x-access-token': localStorage.getItem('Token') }
            }).then((response) => {
                this.getProjects();
            }).catch((error) => {
                console.log(error);
            });
        }
    }

    render() { 
        if (this.state.data.length>0) {
            return(
                <div id="main" className="container-table">  
                    <table className="table">
                        <tr className="linha1">
                            <th className="index">Index</th>
                            <th className="name">Nome</th>
                            <th className="desc">Descrição</th>
                            <th className="membros">Membros</th>
                            <th className="editar">Git</th>
                            <th className="editar">Editar</th>
                            <th className="excluir">Excluir</th>
                        </tr>
                        {this.state.data.map(pj => {
                            return(
                                <tr className="linhas">
                                    <td className="id">{pj.id}</td>
                                    <td className="nome">{pj.name}</td>
                                    <td>{pj.description}</td>
                                    <td>
                                        {/* <tr><span><i class="fas fa-user-plus"></i></span></tr>  */}
                                        
                                        {pj.members.map(mem => {
                                            return(
                                                <div>
                                                    <tr>
                                                        <td>{mem.name}</td>
                                                        <td><span><button className="delete" onClick={()=> this.deleteMember(mem.id)}><i class="fa fa-times"></i></button></span></td>
                                                    </tr>
                                                                                                      
                                                </div>                                                
                                            );
                                        })}
                                        <span className="add-button"><Modal titlebutton={'Adicionar Membro'} action={() => {this.getProjects()}}> <AddMember project={pj.id}/></Modal></span>
                                    </td>
                                    <td><a className="git" target="blank" href={pj.link}><i className="edit2 fab fa-gitlab"/></a></td>                                  
                                    <td><NavLink className="editMember" to={`/editproject/${pj.id}`}><i className="edit2 fas fa-user-edit"/></NavLink></td>
                                    <td><button className="edit" onClick={()=> this.deleteProject(pj.id)}><i className="edit2 far fa-trash-alt"></i></button></td>
                                </tr>
                            );
                        })}
                    </table>
                </div>
            ); 
        } else {
            return (
                <div id="main" className="container-table"> 
                    <div className="loading-box">
                        <div className="loading"/>
                    </div>
                </div>);
        }
    }
}

export default ListProjectsPage;