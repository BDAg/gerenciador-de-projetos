import React, { Component } from 'react';
import Axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './CadastroProjeto.css';

toast.configure()

class CadastroProjetoPage extends Component {

    constructor(props) {
        super(props);

        this.state = { 
            theme: '',
            description: '',
            git: '',
        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
        console.log(this.state.theme);
    }

    addMember(nameMember, termMember, telephoneMember, project_id) {
        Axios({
            method: 'POST',
            url: `http://142.4.197.81:8080/v1/members/${project_id}`,
            headers: { 'x-access-token': localStorage.getItem('Token') },
            data: {
                name: nameMember,
                term: termMember,
                telephone: telephoneMember,
                level_id: '4',
            }
        }).then((response) => {
            console.log(response);
        }).catch((error) => {
            console.log(error);
            toast.error('Erro!');
        })
    }
    addProjeto() {
        Axios({
            method: 'POST',
            url: 'http://142.4.197.81:8080/v1/projects/',
            headers: { 'x-access-token': localStorage.getItem('Token')},
            data: this.state,
        }).then((response) => {
            console.log(response.data.response.insertId);

            if(document.getElementById("name1").value.length > 0) {
                this.addMember(document.getElementById("name1").value,document.getElementById("term1").value, document.getElementById("telephone1").value, response.data.response.insertId);
            }
            
            if(document.getElementById("name2").value.length > 0) {
                this.addMember(document.getElementById("name2").value,document.getElementById("term2").value, document.getElementById("telephone2").value, response.data.response.insertId);
            }
            
            if(document.getElementById("name3").value.length > 0) {
                this.addMember(document.getElementById("name3").value,document.getElementById("term3").value, document.getElementById("telephone3").value, response.data.response.insertId);
            }

            if(document.getElementById("name4").value.length > 0) {
                this.addMember(document.getElementById("name4").value,document.getElementById("term4").value, document.getElementById("telephone4").value, response.data.response.insertId);
            }
            
            if(document.getElementById("name5").value.length > 0) {
                this.addMember(document.getElementById("name5").value,document.getElementById("term5").value, document.getElementById("telephone5").value, response.data.response.insertId);     
            }
            
            if(document.getElementById("name6").value.length > 0) {
                this.addMember(document.getElementById("name6").value,document.getElementById("term6").value, document.getElementById("telephone6").value, response.data.response.insertId);
            }
            toast.success(response.data.message, { position: toast.POSITION.BOTTOM_CENTER });
        }).catch((error) => {
            toast.error('Erro ao adicionar projeto!', { position: toast.POSITION.BOTTOM_CENTER });
        });
    }
    
    render() {
        return(
            <div className="container">
                <div className="form-contact">
                    <div>
                        <h4>Tema</h4>
                        <input placeholder="Tema do projeto" className="form-contact-input" name="theme" size="50" onChange={this.handleChange}/>
                    </div>
                    <div>
                        <h4>Link do Git</h4>
                        <input placeholder="Link do Git" className="form-contact-input" name="git" size="50" onChange={this.handleChange}/>
                    </div>
                    <div>
                        <h4>Descrição</h4>
                        <textarea placeholder="Descreva o conteúdo do projeto" className="form-contact-textarea" name="description" size="50" onChange={this.handleChange}/>
                    </div><br/>
                    <div className="member-form">
                        <h4>Membro 1</h4>
                        <input className="form-contact-input" id="name6" placeholder="Nome" />
                        <div className="form-contact-box">
                            <input className="form-contact-input-telephone" id="telephone6" placeholder="Telefone"/>                  
                                <select id="term6" >
                                    <option selected>Termo</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                        </div>
                    </div>
                    <div className="member-form">
                        <h4>Membro 2</h4>
                        <input className="form-contact-input" id="name1" placeholder="Nome"/>
                        <div className="form-contact-box">
                            <input className="form-contact-input-telephone" id="telephone1" placeholder="Telefone"/>                    
                                <select id="term1">
                                    <option selected>Termo</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                        </div>
                    </div>
                    <div className="member-form">
                        <h4>Membro 3</h4>
                        <input className="form-contact-input" id="name2" placeholder="Nome"/>
                        <div className="form-contact-box">
                            <input className="form-contact-input-telephone" id="telephone2" placeholder="Telefone"/>                  
                                <select id="term2">
                                    <option selected>Termo</option>  
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                        </div>
                    </div>
                    <div className="member-form">
                        <h4>Membro 4</h4>
                        <input className="form-contact-input" id="name3"placeholder="Nome"/>
                            <div className="form-contact-box">
                            <input className="form-contact-input-telephone" id="telephone3"placeholder="Telefone"/>                 
                                <select id="term3">
                                    <option selected>Termo</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                        </div>
                    </div>
                    <div className="member-form">
                        <h4>Membro 5</h4>
                        <input className="form-contact-input" id="name4"placeholder="Nome"/>
                        <div className="form-contact-box">
                            <input className="form-contact-input-telephone" id="telephone4"placeholder="Telefone"/>                 
                                <select id="term4">
                                    <option selected>Termo</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                        </div>
                    </div>
                    <div className="member-form">
                        <h4>Membro 6</h4>
                        <input className="form-contact-input" id="name5"placeholder="Nome"/>
                        <div className="form-contact-box">
                            <input className="form-contact-input-telephone" id="telephone5" placeholder="Telefone"/>              
                                <select id="term5">
                                    <option selected>Termo</option>
                                    <option value="1">1</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                        </div>
                    </div>
                    
                    <div>
                        <button className="form-contact-button"onClick={()=> this.addProjeto()}>Enviar</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default CadastroProjetoPage;