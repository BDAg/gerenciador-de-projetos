import React, { Component } from 'react';
import EditProject from '../components/EditProject/EditProject';

class ProjectEditPage extends Component {
    render() {
        return(
            <EditProject project={this.props.match.params.project_id} history={this.props.history}/>
        );
    }
}

export default ProjectEditPage;