import React, { Component } from 'react';
import Axios from 'axios';
import { ConfirmButton, CancelButton } from '../../components/Button/Button';
import { Input } from '../../components/Input/Input';
import { toast } from 'react-toastify';

import './LoginPage.css';

class LoginPage extends Component {

    authControll() {      
        Axios({
            method: 'post',
            url: 'http://142.4.197.81:8080/v1/auth',
            data: {
                email: document.getElementById("email").value,
                password: document.getElementById("password").value
            }
        }).then((response) => {
            console.log(response);
            localStorage.setItem('Token', response.data.token);
            this.props.history.push('/lista-projetos');
        }).catch((error) => {
            toast.error('Preencha todos os campos!', { position: toast.POSITION.BOTTOM_CENTER });
        })
    }

    render() {
        return (
                <div className="LoginPage">
                    <div className="FormLoginPage">

                        <Input
                            placeholder="Digite seu e-email"
                            type="email"
                            width="100%"
                            id="email"
                            name="email"
                            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                        />

                        <Input
                            placeholder="Digite sua senha"
                            type="password"
                            width="100%"
                            id="password"
                            name="password"
                        />

                        <ConfirmButton
                                text="Iniciar sessão" 
                                onClick={this.authControll.bind(this)} 
                        />

                        <CancelButton
                            text="Cancelar Op."
                            onClick={() => {
                                console.log("My Function")
                            }} 
                        />
                    </div>
                </div>
        );
    }
}

export default LoginPage;